@component('mail::message')
# Hi {{$user->name}}

Thanks for create an account. Please verify it using the next button

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirm Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent




