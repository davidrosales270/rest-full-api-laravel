@component('mail::message')
# Hi {{$user->name}}

Your have changed your email. Please verify your new email using the next button:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Confirm Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent