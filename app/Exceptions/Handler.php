<?php

namespace App\Exceptions;

use Asm89\Stack\CorsService;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Traits\ApiResponser;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\QueryException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      $response = $this->handlerException($request, $exception);

        app(CorsService::class)->addActualRequestHeaders($response, $request);
        return $response;
        
    }

    public function handlerException($request, Exception $exception){
        if($exception instanceof ValidationException){
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        if($exception instanceof ModelNotFoundException){
            $model = strtolower ( class_basename( $exception->getModel() ) );
            return $this->errorResponse("There is not instance of {$model} with the specific id", 404);
        }

        if($exception instanceof AuthenticationException){
            return $this->unauthenticated($request, $exception);
        }

        if($exception instanceof AuthorizationException){
            return $this->errorResponse('Your are not allow permission to this action', 403);
        }

        if($exception instanceof NotFoundHttpException){
            return $this->errorResponse("Url not found", 404);
        }

        if($exception instanceof MethodNotAllowedHttpException){
            return  $this->errorResponse("Request Method is not valid", 405);
        }

        if($exception instanceof HttpException){
            return  $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        if($exception instanceof QueryException){
            $code = $exception->errorInfo[1];
            if($code == 1451){
                return  $this->errorResponse('You can not delete the resource because is related with other', 409);
            }
            return  $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }
        if($exception instanceof TokenMismatchException){
            return redirect()->back()->withInput($request->input());
        }

        if(config(app.debug)){
            return parent::render($request, $exception);
        }
        return $this->error('Unexpected failure, try again', 500);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if($this->isFrontend($request)){
            return redirect()->guest('login');
        }
        return $this->errorResponse('Authetication required',401);
    }

    /**
     * Create a response object from the given validation exception
     * @param \Illuminate\Validation\ValidationException $e
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request){

        $errors = $e->validator->errors()->getMessages();

        if($this->isFrontend($request)){
            return $request->ajax() ? response()->json($errors, 422) : redirect()
                ->back()
                ->withInput($request->input())
                ->withErrors($errors);
        }

        return $this->errorResponse($errors, 422);
    }

    private function isFrontend($request){
        return $request->acceptsHtml() && collect($request->route()->middleware())
            ->contains('web');
    }
}
