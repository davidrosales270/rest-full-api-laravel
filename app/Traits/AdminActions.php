<?php
/**
 * Created by PhpStorm.
 * User: David Rosales
 * Date: 22/2/2019
 * Time: 11:02
 */

namespace App\Traits;


use App\User;

trait AdminActions
{
    private function before(User $user, $ability){
        if($user->isAdmin()){
            return true;
        }
    }
}